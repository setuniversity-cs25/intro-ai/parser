import nltk
import sys
from nltk.tokenize import word_tokenize

TERMINALS = """
Adj -> "country" | "dreadful" | "enigmatical" | "little" | "moist" | "red"
Adv -> "down" | "here" | "never"
Conj -> "and" | "until"
Det -> "a" | "an" | "his" | "my" | "the"
N -> "armchair" | "companion" | "day" | "door" | "hand" | "he" | "himself"
N -> "holmes" | "home" | "i" | "mess" | "paint" | "palm" | "pipe" | "she"
N -> "smile" | "thursday" | "walk" | "we" | "word"
P -> "at" | "before" | "in" | "of" | "on" | "to"
V -> "arrived" | "came" | "chuckled" | "had" | "lit" | "said" | "sat"
V -> "smiled" | "tell" | "were"
"""

NONTERMINALS = """
S -> NP | NP V NP | NP V NP Conj S
NP -> N | Det N | NP P NP | Det Adj N | NP V | NP Adv
V -> V | V P | Adv V
Conj -> Conj | Conj N | Conj V
Adv -> Adv | Adv Conj
Adj -> Adj | Adj Adj
"""

grammar = nltk.CFG.fromstring(NONTERMINALS + TERMINALS)
parser = nltk.ChartParser(grammar)


def main():

    # If filename specified, read sentence from file
    if len(sys.argv) == 2:
        with open(sys.argv[1]) as f:
            s = f.read()

    # Otherwise, get sentence as input
    else:
        s = input("Sentence: ")

    # Convert input into list of words
    s = preprocess(s)

    # Attempt to parse sentence
    try:
        trees = list(parser.parse(s))
    except ValueError as e:
        print(e)
        return
    if not trees:
        print("Could not parse sentence.")
        return

    # Print each tree with noun phrase chunks
    for tree in trees:
        tree.pretty_print()

        print("Noun Phrase Chunks")
        for np in np_chunk(tree):
            print(" ".join(np.flatten()))


def preprocess(sentence):
    """
    Convert `sentence` to a list of its words.
    Pre-process sentence by converting all characters to lowercase
    and removing any word that does not contain at least one alphabetic
    character.
    """
    sentence = sentence.lower()
    words = word_tokenize(sentence)
    filtered_words = [word for word in words if any(c.isalpha() for c in word)]

    return filtered_words


def np_chunk(tree):
    """
    Return a list of all noun phrase chunks in the sentence tree.
    A noun phrase chunk is defined as any subtree of the sentence
    whose label is "NP" that does not itself contain any other
    noun phrases as subtrees.
    """
    np_chunks = []

    # If the current tree is a leaf node, return an empty list
    if isinstance(tree, nltk.Tree) and len(tree) == 1 and isinstance(tree[0], str):
        return np_chunks

    # If the current tree has the label "NP" and does not contain other NP subtrees
    if isinstance(tree, nltk.Tree) and tree.label() == "NP" and not any(
            isinstance(subtree, nltk.Tree) and subtree.label() == "NP" for subtree in tree):
        np_chunks.append(tree)

    # Recursively process each subtree
    for subtree in tree:
        np_chunks.extend(np_chunk(subtree))

    return np_chunks


if __name__ == "__main__":
    main()
